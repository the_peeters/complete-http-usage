import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.page.html',
  styleUrls: ['./update-user.page.scss'],
})
export class UpdateUserPage implements OnInit {
  titlePage: string = '(Default) Update George Bluth';
  user: any;
  update_name:string;
  update_job:string;
  constructor(
    public http: HttpClient,
    public toastController: ToastController,
    private route: ActivatedRoute, 
  )
  { }

  ngOnInit() {
    this.route.params.subscribe((params: any) => {
      if (params['id']) {
        this.http.get("https://reqres.in/api/users/"+params['id']).subscribe(data=>{
          this.titlePage=data['data']['first_name']+" "+data['data']['last_name']+" update";
          this.user= data['data']['id'];
        });
      }
    });
  }
  sendPutData(){
    this.user = (this.user==undefined) ? 1 : this.user;

    if (this.update_name && this.update_job ){
 
      let postData = {
        "name": this.update_name,
        "job": this.update_job
      }
      this.http.put("https://reqres.in/api/users/"+this.user, postData)
      .subscribe(data => {
        const msg = 'Name: '+ data['name']+"\n"+
                    'Job: '+ data['job']+"\n"+
                    'updatedAt at: '+ data['updatedAt']+"\n";
        this.presentSuccessfulToast('User update (PUT Method)',msg);
       }, error => {
        this.presentFailureToast('Could not update the user '+  this.user+".");
        console.log(error);
      });
      
    }else{
      this.presentFailureToast('Cannot update user '+ this.user+' with empty values.')
    }
  }

  sendPatchData(){
    this.user = (this.user==undefined) ? 1 : this.user;
    if (this.update_name && this.update_job ){
      let postData = {
        "name": this.update_name,
        "job": this.update_job
      }
      this.http.patch("https://reqres.in/api/users/"+this.user , postData)
      .subscribe(data => {
        const msg = 'Name: '+ data['name']+"\n"+
                    'Job: '+ data['job']+"\n"+
                    'updatedAt at: '+ data['updatedAt']+"\n";
        this.presentSuccessfulToast('User update (PATCH Method)',msg);
       }, error => {
        this.presentFailureToast('Could not update the user '+ this.user+'.');
        console.log(error);
      });
      
    }else{
      this.presentFailureToast('Cannot update user '+ this.user+' with empty values.')
    }
  }



  async presentFailureToast(msg:string) { 
    const toast = await this.toastController.create({      
      message: msg,      
      duration: 2000    
    });    
    toast.present();  
  }
  async presentSuccessfulToast(header:string,msg:string) {    
    const toast = await this.toastController.create({      
      header: header,      
      message: msg,                                                
      position: 'top',      
      buttons: [        
        {          
          text: 'Done',          
          role: 'cancel',          
          handler: () => {            
            console.log('Done clicked');          
          }        
        }      
      ]    
    });    
    await toast.present();
  }

}



