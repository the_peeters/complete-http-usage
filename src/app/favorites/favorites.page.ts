import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { UsersManagerService } from '../services/managers/users-manager.service';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.page.html',
  styleUrls: ['./favorites.page.scss'],
})
export class FavoritesPage implements OnInit {
  storageItems: Array<any>;
  constructor(
    private storage:Storage,
    private router: Router,
    public provider: UsersManagerService
    
    ) { }
  ngOnInit() { }
  
  async ionViewWillEnter(){ 
    console.log("ionViewWillEnter - Favorites");
    this.storageItems = await this.provider.getFavorites();
    //this.storageItems = await this.provider.getFavorites_ARRAY();  
  }
  
  userClicked(id:any){
    this.router.navigate(["list/detail",id]);
  }

  deleteFavorite(id:any){
    console.log("Removing favorie");
    this.provider.removeFavoriteById(id);
    //this.provider.removeFavoriteById_ARRAY(id);
    window.location.reload();

  }

  deleteAll(){
    this.provider.delete_all_favorites();
    //this.provider.delete_all_favorites_ARRAY();
    window.location.reload();
  }
}
