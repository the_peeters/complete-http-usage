import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-delete-user',
  templateUrl: './delete-user.page.html',
  styleUrls: ['./delete-user.page.scss'],
})
export class DeleteUserPage implements OnInit {
  titlePage: string = '(Default) Delete George Bluth';
  user: Observable<any>;
  id:any;
  constructor(
    public http: HttpClient,
    public toastController: ToastController,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.route.params.subscribe((params: any) => {
      if (params['id']) {
       
        this.id = params['id'];
        this.user = this.http.get("https://reqres.in/api/users/"+params['id']);
        this.user.subscribe(data=>{
          this.titlePage="Delete "+data['data']['first_name']+" "+data['data']['last_name'];
        });
      }
    });
  }
  deleteUser(){
    this.id = (this.id==undefined) ? 1:this.user;
    this.http.delete("https://reqres.in/api/users/"+this.id)
      .subscribe(data => {
        console.log(data);
        this.presentSuccessfulToast('Succesful deletion',"");
       }, error => {
        this.presentFailureToast('Error during the delete process');
        console.log(error);
      });
    

  }
  async presentFailureToast(msg:string) { 
    const toast = await this.toastController.create({      
      message: msg,
      position: 'top',       
      duration: 2000    
    });    
    toast.present();  
  }
  
  async presentSuccessfulToast(header:string,msg:string) {    
    const toast = await this.toastController.create({      
      header: header,      
      message: msg,                                                
      position: 'top',      
      buttons: [        
        {          
          text: 'Done',          
          role: 'cancel',          
          handler: () => {            
            console.log('Clicked done');             
          }        
        }      
      ]    
    });    
    await toast.present();
  }
  

}
