import { Component } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(
    public menu: MenuController,
    private storage:Storage)
   { }
  closeMenu(){
    this.menu.close();
  }
  async ngOnInit(){
    // If usinbg a custom driver;
    //await this.storage.defineDriver(MycustomDriver);
    await this.storage.create();
  }
}
