import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToastController } from '@ionic/angular';
import { FormControl } from '@angular/forms'; // add  ReactiveFormsModule to register-page.modules.ts
import { UsersManagerService } from '../services/managers/users-manager.service';

@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.page.html',
  styleUrls: ['./register-user.page.scss'],
})
export class RegisterUserPage implements OnInit {
  register_password1:string;
  register_password2:string;
  
  users_login:Array<any> = new Array();
  selectControl:FormControl = new FormControl()
  constructor(
    public http: HttpClient,
    public toastController: ToastController,
    public usersManager: UsersManagerService
  ) { }

  ngOnInit() {

  }
  async ionViewWillEnter(){ 
    this.users_login= await this.usersManager.getUsers();
  }
  


  successfulRegister(){
    if (this.register_password1 && this.register_password2 && this.selectControl.value){
      if(this.register_password1 != this.register_password2){
        this.presentFailureToast('Passwords does not match');
        return;
      }
    
      let postData = {
        "email": this.selectControl.value,
        "password":this.register_password2
      }
      console.log(postData)
      this.http.post("https://reqres.in/api/register", postData)
      .subscribe(data => {
        console.log(data);
        const msg='Id: '+ data['id']+"\n"+
                  'Token: '+data['token'];
        this.presentSuccessfulToast('Succesful Registration',msg);
       }, error => {
        this.presentFailureToast('Error during registration');
        console.log(error);
      });
      
    }else{
      this.presentFailureToast('Cannot make successful registration with empty values')
    }
  }

  failureRegister(){
    if(this.selectControl.value ===null ){
      console.log(this.selectControl.value)
      this.presentFailureToast('Cannot make UNsuccessful registration without user email');
      return;
    }
    if (!this.register_password1 && !this.register_password2  ){
      let postData = {
        "email":this.selectControl.value,
      }
      console.log(postData)
      this.http.post("https://reqres.in/api/register", postData)
      .subscribe(data => {
        console.log(data);
        this.presentFailureToast('It was supposed it throws an error');
        
       }, error => {
        const msg='Reason: '+ error['error']['error'];
        this.presentSuccessfulToast('Unsuccesful Registration',msg);
        console.log();
      });
      
    }else{
      this.presentFailureToast('Cannot make UNsuccessful registration with passsword value');
    }
  }



  async presentFailureToast(msg:string) { 
    const toast = await this.toastController.create({      
      message: msg,
      position: 'top',       
      duration: 2000    
    });    
    toast.present();  
  }

  async presentSuccessfulToast(header:string,msg:string) {    
    const toast = await this.toastController.create({      
      header: header,      
      message: msg,                                                
      position: 'top',      
      buttons: [        
        {          
          text: 'Done',          
          role: 'cancel',          
          handler: () => {            
            console.log('Clicked done');          
          }        
        }      
      ]    
    });    
    await toast.present();
  }




 

}
