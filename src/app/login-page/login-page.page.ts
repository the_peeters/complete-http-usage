import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToastController } from '@ionic/angular';
import { FormControl } from '@angular/forms';
import { UsersManagerService } from '../services/managers/users-manager.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.page.html',
  styleUrls: ['./login-page.page.scss'],
})
export class LoginPagePage implements OnInit {
  users_login:Array<any>;

  login_password:string;
  selectControl:FormControl = new FormControl();
  
  constructor(
    public http: HttpClient,
    public toastController: ToastController,
    public usersManager: UsersManagerService
  ) { }

  ngOnInit() {

  }

  async ionViewWillEnter(){ 
    this.users_login= await this.usersManager.getUsers();
  }

  successfulLogin(){

    if (this.login_password && this.selectControl.value ){
      let postData = {
        "email": this.selectControl.value,
        "password":this.login_password
      }
      console.log(postData)
      this.http.post("https://reqres.in/api/login", postData)
      .subscribe(data => {
        console.log(data);
        const msg='Token: '+data['token'];
        this.presentSuccessfulToast('Succesful Login',msg);
       }, error => {
        this.presentFailureToast('Error during registration');
        console.log(error);
      });
      
    }else{
      this.presentFailureToast('Cannot make successful registration with empty values')
    }
  }

  failureLogin(){
    if (this.selectControl.value && !this.login_password ){
      let postData = {
        "email":this.selectControl.value,
      }
      console.log(postData)
      this.http.post("https://reqres.in/api/register", postData)
      .subscribe(data => {
        console.log(data);
        this.presentFailureToast('It was supposed it throws an error');
        
       }, error => {
        const msg='Reason: '+ error['error']['error'];
        this.presentSuccessfulToast('Login failure',msg);
        console.log();
      });
      
    }else{
      this.presentFailureToast('Cannot make UNsuccessful registration with passsword value or without email')
    }
  }



  async presentFailureToast(msg:string) { 
    const toast = await this.toastController.create({      
      message: msg,
      position: 'top',       
      duration: 2000    
    });    
    toast.present();  
  }

  async presentSuccessfulToast(header:string,msg:string) {    
    const toast = await this.toastController.create({      
      header: header,      
      message: msg,                                                
      position: 'top',      
      buttons: [        
        {          
          text: 'Done',          
          role: 'cancel',          
          handler: () => {            
            console.log('Clicked done');          
          }        
        }      
      ]    
    });    
    await toast.present();
  }

}
