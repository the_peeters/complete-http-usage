import { HttpClient } from '@angular/common/http'; //add to componets
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { UsersManagerService } from '../services/managers/users-manager.service';
//flter results
//https://www.positronx.io/how-to-build-list-filtering-and-searching-in-ionic/

@Component({
  selector: 'app-mainpage',
  templateUrl: './mainpage.page.html',
  styleUrls: ['./mainpage.page.scss'],
})
export class MainpagePage implements OnInit {

  listUsers: Array<any>;
  filterTerm: string;

  constructor(
    public toastController: ToastController,
    private router: Router,
    public usersManager: UsersManagerService,
    public http: HttpClient
  ) { }

  ngOnInit() { }

  async ionViewWillEnter() {
    this.listUsers = await this.usersManager.getUsers();
  }

  async markFavorite(obj: any) {
    this.Toast(obj['first_name'] + ' marked as favorite');
    this.usersManager.saveFavorites(obj['id'], obj);
    //this.usersManager.saveFavorites_ARRAY(obj);
  }
  async unmarkFavorite(obj: any) {
    this.Toast(obj['first_name'] + ' removed from favorites');
    this.usersManager.removeFavoriteById(obj['id']);
    //this.usersManager.removeFavoriteById_ARRAY(obj['id'])
  }

  viewDetail(id_user: any) {
    this.router.navigate(["list/detail", id_user]);
  }

  updateThisUser(id_user: any) {
    this.router.navigate(["list/update", id_user]);
  }

  async deleteThisUser(id_user: any) {
    this.router.navigate(["list/delete", id_user]);
  }

  async Toast(msg: string) {
    const toast = await this.toastController.create({
      message: msg,
      position: 'top',
      duration: 2000
    });
    toast.present();
  }

  checkFavorite(id: any) {
    return this.usersManager.exists_in_favorites(id);
    //return this.usersManager.exists_in_favorites_ARRAY(id);
  }

}
