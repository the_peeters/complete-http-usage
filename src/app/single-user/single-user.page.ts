import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { UsersManagerService } from '../services/managers/users-manager.service';


@Component({
  selector: 'app-single-user',
  templateUrl: './single-user.page.html',
  styleUrls: ['./single-user.page.scss'],
})
export class SingleUserPage implements OnInit {
  titlePage: string = 'Default detail user: George Bluth';
  selectedUser: any;
  observer: Observable<any>;
  
  constructor(
    private route: ActivatedRoute,
    public http: HttpClient,
    public usersManager: UsersManagerService
    ) { }

  ngOnInit() { }
  ionViewDidLoad(){ } 
  async ionViewWillEnter() {

    this.route.params.subscribe((params: any) => {

      let user_id = (params['id']) ? params['id'] : 1 ;
      let default_=(params['id']) ? false : true;
      
      
      this.observer = this.http.get("https://reqres.in/api/users/"+user_id);
     

      

      this.observer.subscribe(data=>{
        if(default_==true){
         this.titlePage='Default detail user: George Bluth';
        }else{
          this.titlePage=data['data']['first_name']+" "+data['data']['last_name'];
        }
        this.selectedUser = data['data'];
          
      });

      /*
      this.usersManager.getUsers().then(arrayData =>{ 
        this.selectedUser = arrayData.filter(usrObj => usrObj['id']==user_id)[0];
        if(default_==true){
          this.titlePage='Default detail user: George Bluth';
        }else{
          this.titlePage=this.selectedUser['first_name']+" "+this.selectedUser['last_name'];
        }
      });
       */


    });
  }
  
  //ionViewDidEnter(){ } 
  //ionViewWillLeave(){ } 
  //ionViewDidLeave(){ } 
  //ionViewWillUnload(){ }

}
