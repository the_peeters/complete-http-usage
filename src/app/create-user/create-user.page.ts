import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToastController } from '@ionic/angular';
@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.page.html',
  styleUrls: ['./create-user.page.scss'],
})
export class CreateUserPage implements OnInit {
  create_name:string;
  create_job:string;

  constructor(
    public http: HttpClient,
    public toastController: ToastController
  ) {
   }

  ngOnInit() {
  }
  sendPostData(){
    if (this.create_name && this.create_job ){
      let postData = {
        "name": this.create_name,
        "job": this.create_job
      }
      this.http.post("https://reqres.in/api/users", postData)
      .subscribe(data => {
        console.log(data);
        this.presentToastWithOptions(data)
       }, error => {
        this.presentToast('Could not create the new user.')
        console.log(error);
      });
      
    }else{
      this.presentToast('Cannot create user with empty values.')
    }
  }
 
  async presentToast(msg:string) { 
    const toast = await this.toastController.create({      
      message: msg,      
      duration: 2000    
    });    
    toast.present();  
  }

  async presentToastWithOptions(data:any) {    
    const toast = await this.toastController.create({      
      header: 'Created new user.',      
      message: 'Id: '+ data['id']+"\n"+
               'Name: '+ data['name']+"\n"+
               'Job: '+ data['job']+"\n"+
               'Created at: '+ data['createdAt']+"\n",                                                
      position: 'top',      
      buttons: [        
        {          
          text: 'Done',          
          role: 'cancel',          
          handler: () => {            
            console.log('Clicked done');              
          }        
        }      
      ]    
    });    
    await toast.present();
  }

}
