import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class UsersManagerService {
  public usersCache: Array<any>;

  public favoritesCacheMap: Map<any, any>;

  public usersFavoriteIds: Array<Number>;
  public usersFavoriteObjs: Array<any>;

  constructor(public http: HttpClient, private storage: Storage) { }

  async getUsers() {
    if (this.usersCache == undefined) {
      this.usersCache = new Array();
      await this.http.get("https://reqres.in/api/users?per_page=12").toPromise().then(data => {
        console.log("Getting from api");
        let arr = data['data'];
        arr.forEach(item => {
          this.usersCache.push(item);
        });
      });
    } else {
      console.log("Getting from cached ");
    }

    return this.usersCache;
  }

  //--------- HASHMAP ------------------
  async getFavorites() {
    if (this.favoritesCacheMap == undefined) {
      await this.storage.get("fav").then(data => {
        if (data) {
          console.log("setting into cached Favorites map");
          this.favoritesCacheMap = data;
        } else {
          console.log("Created a new favorites cache map");
          this.favoritesCacheMap = new Map();
        }
      });
    }
    return Array.from(this.favoritesCacheMap.values());

  }

  saveFavorites(id: Number, value: any) {
  
    let key="u"+id;

    if (this.favoritesCacheMap == undefined) {
      console.log("Created a new favorites cache map");
      this.favoritesCacheMap = new Map();
    } else {
      console.log("Saved to favorites cache");
    }
    this.favoritesCacheMap.set(key, value);
    this.storage.set("fav", this.favoritesCacheMap);

  }

  removeFavoriteById(userid: Number) { 
    if (this.favoritesCacheMap != undefined) {
      console.log("Deleted user from favorites cache");
      this.favoritesCacheMap.delete("u"+userid);
      this.storage.set("fav", this.favoritesCacheMap);
    }
  }

  exists_in_favorites(id: Number) {

    if (this.favoritesCacheMap == undefined) {
      return false;
    } else {
      return this.favoritesCacheMap.has("u" + id);
    }

  }

  delete_all_favorites(){
    this.storage.remove("fav");
  }


  //--------- ARRAY ------------------
  async getFavorites_ARRAY() {
    if (this.usersFavoriteIds ==undefined && this.usersFavoriteIds ==undefined ) {
      
      await this.storage.get("fav_ARRAY_IDS").then(array_ids => {
        if (array_ids) {
          console.log("setting into cached Favorites IDS ARRAY");
          this.usersFavoriteIds = array_ids;
        } else {
          console.log("Created a new fFavorites IDS ARRAY");
          this.usersFavoriteIds = new Array();
        }
      });
      await this.storage.get("fav_ARRAY_OBJS").then(array_objs => {
        if (array_objs) {
          console.log("setting into cached Favorites OBJS ARRAY");
          this.usersFavoriteObjs = array_objs;
        } else {
          console.log("Created a new fFavorites OBJS ARRAY");
          this.usersFavoriteObjs = new Array();
        }
      });
    }
    return this.usersFavoriteObjs;

  }

  saveFavorites_ARRAY(value: any) {
    if (this.usersFavoriteIds ==undefined && this.usersFavoriteObjs ==undefined ){
      console.log("Created a new favorites cache ARRAYS");
      this.usersFavoriteIds = new Array();
      this.usersFavoriteObjs = new Array();
    }else{
      console.log("Saved to favorites cach ARRAYS");
    }
    this.usersFavoriteIds.push(value['id']);
    this.usersFavoriteObjs.push(value);
    this.storage.set("fav_ARRAY_IDS", this.usersFavoriteIds);
    this.storage.set("fav_ARRAY_OBJS", this.usersFavoriteObjs);
  }

  removeFavoriteById_ARRAY(userid: Number) { 
    
    if(this.usersFavoriteIds!=undefined && this.usersFavoriteObjs !=undefined ){
      console.log("Deleted user from favorites cache ARRAYS");
      this.usersFavoriteIds = this.usersFavoriteIds.filter(favId => favId != userid);
      this.usersFavoriteObjs = this.usersFavoriteObjs.filter(favObj => favObj['id'] != userid);

      this.storage.set("fav_ARRAY_IDS",this.usersFavoriteIds);
      this.storage.set("fav_ARRAY_OBJS", this.usersFavoriteObjs);
    }
  }

  exists_in_favorites_ARRAY(idFind: Number){
    if(this.usersFavoriteIds==undefined){
      return false;
    }else{
      const result = this.usersFavoriteIds.filter(id => idFind == id);
      return (result.length==0) ? false : true;
    }

  }

  delete_all_favorites_ARRAY(){
    this.storage.remove("fav_ARRAY_IDS");
    this.storage.remove("fav_ARRAY_OBJS");
  }
}

