(self["webpackChunkIonicApiRequest"] = self["webpackChunkIonicApiRequest"] || []).push([["src_app_update-user_update-user_module_ts"],{

/***/ 67577:
/*!***********************************************************!*\
  !*** ./src/app/update-user/update-user-routing.module.ts ***!
  \***********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UpdateUserPageRoutingModule": function() { return /* binding */ UpdateUserPageRoutingModule; }
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 29535);
/* harmony import */ var _update_user_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./update-user.page */ 13942);




const routes = [
    {
        path: '',
        component: _update_user_page__WEBPACK_IMPORTED_MODULE_0__.UpdateUserPage
    }
];
let UpdateUserPageRoutingModule = class UpdateUserPageRoutingModule {
};
UpdateUserPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], UpdateUserPageRoutingModule);



/***/ }),

/***/ 20325:
/*!***************************************************!*\
  !*** ./src/app/update-user/update-user.module.ts ***!
  \***************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UpdateUserPageModule": function() { return /* binding */ UpdateUserPageModule; }
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 16274);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 93324);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 34595);
/* harmony import */ var _update_user_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./update-user-routing.module */ 67577);
/* harmony import */ var _update_user_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./update-user.page */ 13942);







let UpdateUserPageModule = class UpdateUserPageModule {
};
UpdateUserPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _update_user_routing_module__WEBPACK_IMPORTED_MODULE_0__.UpdateUserPageRoutingModule
        ],
        declarations: [_update_user_page__WEBPACK_IMPORTED_MODULE_1__.UpdateUserPage]
    })
], UpdateUserPageModule);



/***/ }),

/***/ 13942:
/*!*************************************************!*\
  !*** ./src/app/update-user/update-user.page.ts ***!
  \*************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UpdateUserPage": function() { return /* binding */ UpdateUserPage; }
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _raw_loader_update_user_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./update-user.page.html */ 93875);
/* harmony import */ var _update_user_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./update-user.page.scss */ 31529);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ 31887);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 29535);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ 34595);







let UpdateUserPage = class UpdateUserPage {
    constructor(http, toastController, route) {
        this.http = http;
        this.toastController = toastController;
        this.route = route;
        this.titlePage = '(Default) Update George Bluth';
    }
    ngOnInit() {
        this.route.params.subscribe((params) => {
            if (params['id']) {
                this.http.get("https://reqres.in/api/users/" + params['id']).subscribe(data => {
                    this.titlePage = data['data']['first_name'] + " " + data['data']['last_name'] + " update";
                    this.user = data['data']['id'];
                });
            }
        });
    }
    sendPutData() {
        this.user = (this.user == undefined) ? 1 : this.user;
        if (this.update_name && this.update_job) {
            let postData = {
                "name": this.update_name,
                "job": this.update_job
            };
            this.http.put("https://reqres.in/api/users/" + this.user, postData)
                .subscribe(data => {
                const msg = 'Name: ' + data['name'] + "\n" +
                    'Job: ' + data['job'] + "\n" +
                    'updatedAt at: ' + data['updatedAt'] + "\n";
                this.presentSuccessfulToast('User update (PUT Method)', msg);
            }, error => {
                this.presentFailureToast('Could not update the user ' + this.user + ".");
                console.log(error);
            });
        }
        else {
            this.presentFailureToast('Cannot update user ' + this.user + ' with empty values.');
        }
    }
    sendPatchData() {
        this.user = (this.user == undefined) ? 1 : this.user;
        if (this.update_name && this.update_job) {
            let postData = {
                "name": this.update_name,
                "job": this.update_job
            };
            this.http.patch("https://reqres.in/api/users/" + this.user, postData)
                .subscribe(data => {
                const msg = 'Name: ' + data['name'] + "\n" +
                    'Job: ' + data['job'] + "\n" +
                    'updatedAt at: ' + data['updatedAt'] + "\n";
                this.presentSuccessfulToast('User update (PATCH Method)', msg);
            }, error => {
                this.presentFailureToast('Could not update the user ' + this.user + '.');
                console.log(error);
            });
        }
        else {
            this.presentFailureToast('Cannot update user ' + this.user + ' with empty values.');
        }
    }
    presentFailureToast(msg) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__awaiter)(this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: msg,
                duration: 2000
            });
            toast.present();
        });
    }
    presentSuccessfulToast(header, msg) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__awaiter)(this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                header: header,
                message: msg,
                position: 'top',
                buttons: [
                    {
                        text: 'Done',
                        role: 'cancel',
                        handler: () => {
                            console.log('Done clicked');
                        }
                    }
                ]
            });
            yield toast.present();
        });
    }
};
UpdateUserPage.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__.HttpClient },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__.ToastController },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__.ActivatedRoute }
];
UpdateUserPage = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-update-user',
        template: _raw_loader_update_user_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_update_user_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], UpdateUserPage);



/***/ }),

/***/ 31529:
/*!***************************************************!*\
  !*** ./src/app/update-user/update-user.page.scss ***!
  \***************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container {\n  margin: auto;\n  width: 75%;\n  padding: 16px;\n  background-color: white;\n}\n\n.email-select {\n  background: #f1f1f1;\n  border: none;\n  padding: 15px;\n  margin: 5px 0 22px 0;\n  width: 100%;\n}\n\ninput[type=text] {\n  width: 100%;\n  padding: 15px;\n  margin: 5px 0 22px 0;\n  display: inline-block;\n  border: none;\n  background: #f1f1f1;\n}\n\ninput[type=password] {\n  width: 100%;\n  padding: 15px;\n  margin: 5px 0 22px 0;\n  display: inline-block;\n  border: none;\n  background: #f1f1f1;\n}\n\ninput[type=password]:focus {\n  background: #ddd;\n}\n\n.loginButton {\n  color: white;\n  padding: 16px 20px;\n  margin: 8px 0;\n  border: none;\n  cursor: pointer;\n  width: 100%;\n  opacity: 0.8;\n  font-style: normal;\n  font-size: large;\n}\n\n.red {\n  background-color: #Eb445A;\n}\n\n.green {\n  background-color: #04AA6D;\n}\n\n.round {\n  border-radius: 8px;\n}\n\n.red:hover, .green:hover {\n  opacity: 1;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInVwZGF0ZS11c2VyLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFlBQUE7RUFDQSxVQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0FBQ0o7O0FBSUU7RUFDRSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0Esb0JBQUE7RUFDQSxXQUFBO0FBREo7O0FBSUE7RUFDSSxXQUFBO0VBQ0EsYUFBQTtFQUNBLG9CQUFBO0VBQ0EscUJBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7QUFESjs7QUFJRTtFQUNJLFdBQUE7RUFDQSxhQUFBO0VBQ0Esb0JBQUE7RUFDQSxxQkFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtBQUROOztBQUlFO0VBQ0UsZ0JBQUE7QUFESjs7QUFLRTtFQUNFLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQUZKOztBQUtFO0VBQ0UseUJBQUE7QUFGSjs7QUFLRTtFQUNFLHlCQUFBO0FBRko7O0FBS0U7RUFDRSxrQkFBQTtBQUZKOztBQUtFO0VBQ0UsVUFBQTtBQUZKIiwiZmlsZSI6InVwZGF0ZS11c2VyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb250YWluZXJ7XG4gICAgbWFyZ2luOiBhdXRvO1xuICAgIHdpZHRoOiA3NSU7XG4gICAgcGFkZGluZzogMTZweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgIFxuICB9XG4gIFxuICBcbiAgLmVtYWlsLXNlbGVjdHtcbiAgICBiYWNrZ3JvdW5kOiAjZjFmMWYxO1xuICAgIGJvcmRlcjogbm9uZTtcbiAgICBwYWRkaW5nOiAxNXB4O1xuICAgIG1hcmdpbjogNXB4IDAgMjJweCAwO1xuICAgIHdpZHRoOjEwMCU7XG4gIH1cbiAgXG5pbnB1dFt0eXBlPXRleHRdIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwYWRkaW5nOiAxNXB4O1xuICAgIG1hcmdpbjogNXB4IDAgMjJweCAwO1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICBib3JkZXI6IG5vbmU7XG4gICAgYmFja2dyb3VuZDogI2YxZjFmMTtcbn1cblxuICBpbnB1dFt0eXBlPXBhc3N3b3JkXSB7XG4gICAgICB3aWR0aDogMTAwJTtcbiAgICAgIHBhZGRpbmc6IDE1cHg7XG4gICAgICBtYXJnaW46IDVweCAwIDIycHggMDtcbiAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgIGJvcmRlcjogbm9uZTtcbiAgICAgIGJhY2tncm91bmQ6ICNmMWYxZjE7XG4gIH1cbiAgXG4gIGlucHV0W3R5cGU9cGFzc3dvcmRdOmZvY3Vze1xuICAgIGJhY2tncm91bmQ6ICNkZGQ7XG4gIH1cbiAgXG4gIFxuICAubG9naW5CdXR0b257XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIHBhZGRpbmc6IDE2cHggMjBweDtcbiAgICBtYXJnaW46IDhweCAwO1xuICAgIGJvcmRlcjogbm9uZTtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgb3BhY2l0eTogMC44O1xuICAgIGZvbnQtc3R5bGU6bm9ybWFsO1xuICAgIGZvbnQtc2l6ZTogbGFyZ2U7XG4gIH1cbiAgXG4gIC5yZWR7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0ViNDQ1QTtcbiAgfVxuICBcbiAgLmdyZWVue1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMwNEFBNkQ7XG4gIH1cbiAgXG4gIC5yb3VuZHtcbiAgICBib3JkZXItcmFkaXVzOiA4cHg7XG4gIH1cbiAgXG4gIC5yZWQ6aG92ZXIsLmdyZWVuOmhvdmVye1xuICAgIG9wYWNpdHk6IDE7XG4gIH1cbiAgXG4gIFxuICBcbiAgXG4gIFxuICBcbiAgIl19 */");

/***/ }),

/***/ 93875:
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/update-user/update-user.page.html ***!
  \*****************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar  color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title >User update</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div>\n    <div class=\"container\">\n      <h1>{{titlePage}}</h1>\n      <p>Please fill this form to update this user</p>\n      <hr>\n      <!--\n      <label for=\"email\"><b>Email</b></label>\n      <div>\n        <select class=\"email-select\" [formControl]=\"selectControl\">\n          <option value={{user.email}} *ngFor=\"let user of users_login\" > {{user.email}}</option>\n        </select>\n    \n      </div> \n      -->\n\n\n      \n      <!--\n      <label for=\"name_update\"><b>Name Update</b></label>\n      <input type=\"text\" placeholder=\"new name\" required [(ngModel)]=\"update_name\" [ngModelOptions]=\"{standalone: true}\"> \n      -->\n      <ion-item>\n        <ion-label position=\"floating\">Name update</ion-label>\n        <ion-input type=\"text\" [(ngModel)]=\"update_name\"></ion-input>\n      </ion-item>\n      <!--  \n      <label for=\"job_update\"><b>Job Update</b></label>\n      <input type=\"text\" placeholder=\"job update\" required [(ngModel)]=\"update_job\" [ngModelOptions]=\"{standalone: true}\">\n      -->\n      <ion-item>\n        <ion-label position=\"floating\">Job update</ion-label>\n        <ion-input [(ngModel)]=\"update_job\"></ion-input>\n      </ion-item>\n\n      <hr>\n      <div>\n        <ion-button (click)=\"sendPutData()\">\n          Update (PUT)\n        </ion-button>\n        <ion-button (click)=\"sendPatchData()\">\n          Update (PATCH)\n        </ion-button>\n      </div>\n    </div>\n  </div>\n</ion-content>\n\n");

/***/ })

}]);
//# sourceMappingURL=src_app_update-user_update-user_module_ts-es2015.js.map