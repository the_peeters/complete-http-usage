(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (self["webpackChunkIonicApiRequest"] = self["webpackChunkIonicApiRequest"] || []).push([["src_app_delete-user_delete-user_module_ts"], {
    /***/
    3148:
    /*!***********************************************************!*\
      !*** ./src/app/delete-user/delete-user-routing.module.ts ***!
      \***********************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "DeleteUserPageRoutingModule": function DeleteUserPageRoutingModule() {
          return (
            /* binding */
            _DeleteUserPageRoutingModule
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! tslib */
      61855);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      42741);
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      29535);
      /* harmony import */


      var _delete_user_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./delete-user.page */
      72743);

      var routes = [{
        path: '',
        component: _delete_user_page__WEBPACK_IMPORTED_MODULE_0__.DeleteUserPage
      }];

      var _DeleteUserPageRoutingModule = function DeleteUserPageRoutingModule() {
        _classCallCheck(this, DeleteUserPageRoutingModule);
      };

      _DeleteUserPageRoutingModule = (0, tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule]
      })], _DeleteUserPageRoutingModule);
      /***/
    },

    /***/
    11059:
    /*!***************************************************!*\
      !*** ./src/app/delete-user/delete-user.module.ts ***!
      \***************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "DeleteUserPageModule": function DeleteUserPageModule() {
          return (
            /* binding */
            _DeleteUserPageModule
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! tslib */
      61855);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      42741);
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/common */
      16274);
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/forms */
      93324);
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic/angular */
      34595);
      /* harmony import */


      var _delete_user_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./delete-user-routing.module */
      3148);
      /* harmony import */


      var _delete_user_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./delete-user.page */
      72743);

      var _DeleteUserPageModule = function DeleteUserPageModule() {
        _classCallCheck(this, DeleteUserPageModule);
      };

      _DeleteUserPageModule = (0, tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule, _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule, _delete_user_routing_module__WEBPACK_IMPORTED_MODULE_0__.DeleteUserPageRoutingModule],
        declarations: [_delete_user_page__WEBPACK_IMPORTED_MODULE_1__.DeleteUserPage]
      })], _DeleteUserPageModule);
      /***/
    },

    /***/
    72743:
    /*!*************************************************!*\
      !*** ./src/app/delete-user/delete-user.page.ts ***!
      \*************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "DeleteUserPage": function DeleteUserPage() {
          return (
            /* binding */
            _DeleteUserPage
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! tslib */
      61855);
      /* harmony import */


      var _raw_loader_delete_user_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! !raw-loader!./delete-user.page.html */
      93476);
      /* harmony import */


      var _delete_user_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./delete-user.page.scss */
      53100);
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/common/http */
      31887);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/core */
      42741);
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/router */
      29535);
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      34595);

      var _DeleteUserPage = /*#__PURE__*/function () {
        function DeleteUserPage(http, toastController, route) {
          _classCallCheck(this, DeleteUserPage);

          this.http = http;
          this.toastController = toastController;
          this.route = route;
          this.titlePage = '(Default) Delete George Bluth';
        }

        _createClass(DeleteUserPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this = this;

            this.route.params.subscribe(function (params) {
              if (params['id']) {
                _this.id = params['id'];
                _this.user = _this.http.get("https://reqres.in/api/users/" + params['id']);

                _this.user.subscribe(function (data) {
                  _this.titlePage = "Delete " + data['data']['first_name'] + " " + data['data']['last_name'];
                });
              }
            });
          }
        }, {
          key: "deleteUser",
          value: function deleteUser() {
            var _this2 = this;

            this.id = this.id == undefined ? 1 : this.user;
            this.http["delete"]("https://reqres.in/api/users/" + this.id).subscribe(function (data) {
              console.log(data);

              _this2.presentSuccessfulToast('Succesful deletion', "");
            }, function (error) {
              _this2.presentFailureToast('Error during the delete process');

              console.log(error);
            });
          }
        }, {
          key: "presentFailureToast",
          value: function presentFailureToast(msg) {
            return (0, tslib__WEBPACK_IMPORTED_MODULE_2__.__awaiter)(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var toast;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.toastController.create({
                        message: msg,
                        position: 'top',
                        duration: 2000
                      });

                    case 2:
                      toast = _context.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "presentSuccessfulToast",
          value: function presentSuccessfulToast(header, msg) {
            return (0, tslib__WEBPACK_IMPORTED_MODULE_2__.__awaiter)(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var toast;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return this.toastController.create({
                        header: header,
                        message: msg,
                        position: 'top',
                        buttons: [{
                          text: 'Done',
                          role: 'cancel',
                          handler: function handler() {
                            console.log('Cancel clicked');
                          }
                        }]
                      });

                    case 2:
                      toast = _context2.sent;
                      _context2.next = 5;
                      return toast.present();

                    case 5:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }]);

        return DeleteUserPage;
      }();

      _DeleteUserPage.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__.HttpClient
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__.ToastController
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_5__.ActivatedRoute
        }];
      };

      _DeleteUserPage = (0, tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-delete-user',
        template: _raw_loader_delete_user_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_delete_user_page_scss__WEBPACK_IMPORTED_MODULE_1__["default"]]
      })], _DeleteUserPage);
      /***/
    },

    /***/
    53100:
    /*!***************************************************!*\
      !*** ./src/app/delete-user/delete-user.page.scss ***!
      \***************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJkZWxldGUtdXNlci5wYWdlLnNjc3MifQ== */";
      /***/
    },

    /***/
    93476:
    /*!*****************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/delete-user/delete-user.page.html ***!
      \*****************************************************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title >{{titlePage}}</ion-title>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  \n\n\n  \n<ion-content>\n  <ion-item>\n    <ion-button fill=\"solid\" color=\"primary\" routerLink=\"/mainpage\"> Cancel </ion-button>\n    <ion-button fill=\"solid\" color=\"danger\" (click)=\"deleteUser()\"> Delete </ion-button>\n  </ion-item>\n</ion-content>\n";
      /***/
    }
  }]);
})();
//# sourceMappingURL=src_app_delete-user_delete-user_module_ts-es5.js.map