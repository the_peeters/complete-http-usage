(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (self["webpackChunkIonicApiRequest"] = self["webpackChunkIonicApiRequest"] || []).push([["src_app_favorites_favorites_module_ts"], {
    /***/
    42393:
    /*!*******************************************************!*\
      !*** ./src/app/favorites/favorites-routing.module.ts ***!
      \*******************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "FavoritesPageRoutingModule": function FavoritesPageRoutingModule() {
          return (
            /* binding */
            _FavoritesPageRoutingModule
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! tslib */
      61855);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      42741);
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      29535);
      /* harmony import */


      var _favorites_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./favorites.page */
      67310);

      var routes = [{
        path: '',
        component: _favorites_page__WEBPACK_IMPORTED_MODULE_0__.FavoritesPage
      }];

      var _FavoritesPageRoutingModule = function FavoritesPageRoutingModule() {
        _classCallCheck(this, FavoritesPageRoutingModule);
      };

      _FavoritesPageRoutingModule = (0, tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule]
      })], _FavoritesPageRoutingModule);
      /***/
    },

    /***/
    16547:
    /*!***********************************************!*\
      !*** ./src/app/favorites/favorites.module.ts ***!
      \***********************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "FavoritesPageModule": function FavoritesPageModule() {
          return (
            /* binding */
            _FavoritesPageModule
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! tslib */
      61855);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      42741);
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/common */
      16274);
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/forms */
      93324);
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic/angular */
      34595);
      /* harmony import */


      var _favorites_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./favorites-routing.module */
      42393);
      /* harmony import */


      var _favorites_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./favorites.page */
      67310);

      var _FavoritesPageModule = function FavoritesPageModule() {
        _classCallCheck(this, FavoritesPageModule);
      };

      _FavoritesPageModule = (0, tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule, _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule, _favorites_routing_module__WEBPACK_IMPORTED_MODULE_0__.FavoritesPageRoutingModule],
        declarations: [_favorites_page__WEBPACK_IMPORTED_MODULE_1__.FavoritesPage]
      })], _FavoritesPageModule);
      /***/
    },

    /***/
    67310:
    /*!*********************************************!*\
      !*** ./src/app/favorites/favorites.page.ts ***!
      \*********************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "FavoritesPage": function FavoritesPage() {
          return (
            /* binding */
            _FavoritesPage
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! tslib */
      61855);
      /* harmony import */


      var _raw_loader_favorites_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! !raw-loader!./favorites.page.html */
      38796);
      /* harmony import */


      var _favorites_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./favorites.page.scss */
      87662);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/core */
      42741);
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/router */
      29535);
      /* harmony import */


      var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/storage */
      72604);
      /* harmony import */


      var _services_managers_users_manager_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../services/managers/users-manager.service */
      29022);

      var _FavoritesPage = /*#__PURE__*/function () {
        function FavoritesPage(storage, router, provider) {
          _classCallCheck(this, FavoritesPage);

          this.storage = storage;
          this.router = router;
          this.provider = provider;
          this.storageItems = [];
        }

        _createClass(FavoritesPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            console.log("ngOnInit");
            this.storageItems = [];
          }
        }, {
          key: "ionViewWillEnter",
          value: function ionViewWillEnter() {
            return (0, tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      console.log("ionViewWillEnter - Favorites");
                      _context.next = 3;
                      return this.provider.getFavorites();

                    case 3:
                      this.storageItems = _context.sent;

                    case 4:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "ionViewDidEnter",
          value: function ionViewDidEnter() {
            console.log("ionViewDidEnter");
          }
        }, {
          key: "ionViewWillLeave",
          value: function ionViewWillLeave() {
            console.log("ionViewWillLeave");
          }
        }, {
          key: "ionViewDidLeave",
          value: function ionViewDidLeave() {
            console.log("ionViewDidLeave");
          }
        }, {
          key: "userClicked",
          value: function userClicked(id) {
            this.router.navigate(["list/detail", id]);
          }
        }, {
          key: "deleteFavorite",
          value: function deleteFavorite(id) {
            console.log("Removing favorie");
            this.provider.removeFavoriteById("u" + id);
            window.location.reload();
          }
        }, {
          key: "deleteAll",
          value: function deleteAll() {
            this.storage.remove("fav");
            window.location.reload();
          }
        }]);

        return FavoritesPage;
      }();

      _FavoritesPage.ctorParameters = function () {
        return [{
          type: _ionic_storage__WEBPACK_IMPORTED_MODULE_2__.Storage
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_5__.Router
        }, {
          type: _services_managers_users_manager_service__WEBPACK_IMPORTED_MODULE_3__.UsersManagerService
        }];
      };

      _FavoritesPage = (0, tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-favorites',
        template: _raw_loader_favorites_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_favorites_page_scss__WEBPACK_IMPORTED_MODULE_1__["default"]]
      })], _FavoritesPage);
      /***/
    },

    /***/
    87662:
    /*!***********************************************!*\
      !*** ./src/app/favorites/favorites.page.scss ***!
      \***********************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJmYXZvcml0ZXMucGFnZS5zY3NzIn0= */";
      /***/
    },

    /***/
    38796:
    /*!*************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/favorites/favorites.page.html ***!
      \*************************************************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Favorites </ion-title>\n    <ion-button fill=\"solid\" color=\"danger\" slot=\"end\" (click)=deleteAll()>Delete all favorites</ion-button>\n    \n  </ion-toolbar>\n</ion-header>\n\n\n<ion-content class=\"bg-generic\">\n  <div>\n    <ion-list *ngFor=\"let item of storageItems\">\n      <ion-item>\n        <ion-avatar>\n          <img src=\"{{item.avatar}}\">\n        </ion-avatar>\n        <ion-label>{{item.first_name}} {{item.last_name}}</ion-label>\n        <ion-button fill=\"solid\" color=\"primary\" slot=\"end\" (click)=userClicked(item.id)>Open detail</ion-button>\n        <ion-button fill=\"outline\" color=\"danger\" slot=\"end\" (click)=deleteFavorite(item.id)>Delete favorite</ion-button>\n      </ion-item>\n    </ion-list>\n  </div>\n\n\n</ion-content>\n";
      /***/
    }
  }]);
})();
//# sourceMappingURL=src_app_favorites_favorites_module_ts-es5.js.map