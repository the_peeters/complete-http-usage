(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (self["webpackChunkIonicApiRequest"] = self["webpackChunkIonicApiRequest"] || []).push([["src_app_create-user_create-user_module_ts"], {
    /***/
    84009:
    /*!***********************************************************!*\
      !*** ./src/app/create-user/create-user-routing.module.ts ***!
      \***********************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "CreateUserPageRoutingModule": function CreateUserPageRoutingModule() {
          return (
            /* binding */
            _CreateUserPageRoutingModule
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! tslib */
      61855);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      42741);
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      29535);
      /* harmony import */


      var _create_user_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./create-user.page */
      47596);

      var routes = [{
        path: '',
        component: _create_user_page__WEBPACK_IMPORTED_MODULE_0__.CreateUserPage
      }];

      var _CreateUserPageRoutingModule = function CreateUserPageRoutingModule() {
        _classCallCheck(this, CreateUserPageRoutingModule);
      };

      _CreateUserPageRoutingModule = (0, tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule]
      })], _CreateUserPageRoutingModule);
      /***/
    },

    /***/
    17402:
    /*!***************************************************!*\
      !*** ./src/app/create-user/create-user.module.ts ***!
      \***************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "CreateUserPageModule": function CreateUserPageModule() {
          return (
            /* binding */
            _CreateUserPageModule
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! tslib */
      61855);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      42741);
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/common */
      16274);
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/forms */
      93324);
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic/angular */
      34595);
      /* harmony import */


      var _create_user_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./create-user-routing.module */
      84009);
      /* harmony import */


      var _create_user_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./create-user.page */
      47596);

      var _CreateUserPageModule = function CreateUserPageModule() {
        _classCallCheck(this, CreateUserPageModule);
      };

      _CreateUserPageModule = (0, tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule, _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule, _create_user_routing_module__WEBPACK_IMPORTED_MODULE_0__.CreateUserPageRoutingModule],
        declarations: [_create_user_page__WEBPACK_IMPORTED_MODULE_1__.CreateUserPage]
      })], _CreateUserPageModule);
      /***/
    },

    /***/
    47596:
    /*!*************************************************!*\
      !*** ./src/app/create-user/create-user.page.ts ***!
      \*************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "CreateUserPage": function CreateUserPage() {
          return (
            /* binding */
            _CreateUserPage
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! tslib */
      61855);
      /* harmony import */


      var _raw_loader_create_user_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! !raw-loader!./create-user.page.html */
      30671);
      /* harmony import */


      var _create_user_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./create-user.page.scss */
      45831);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/core */
      42741);
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/common/http */
      31887);
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      34595);

      var _CreateUserPage = /*#__PURE__*/function () {
        function CreateUserPage(http, toastController) {
          _classCallCheck(this, CreateUserPage);

          this.http = http;
          this.toastController = toastController;
        }

        _createClass(CreateUserPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "sendPostData",
          value: function sendPostData() {
            var _this = this;

            if (this.create_name && this.create_job) {
              var postData = {
                "name": this.create_name,
                "job": this.create_job
              };
              this.http.post("https://reqres.in/api/users", postData).subscribe(function (data) {
                console.log(data);

                _this.presentToastWithOptions(data);
              }, function (error) {
                _this.presentToast('Could not create the new user.');

                console.log(error);
              });
            } else {
              this.presentToast('Cannot create user with empty values.');
            }
          }
        }, {
          key: "presentToast",
          value: function presentToast(msg) {
            return (0, tslib__WEBPACK_IMPORTED_MODULE_2__.__awaiter)(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var toast;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.toastController.create({
                        message: msg,
                        duration: 2000
                      });

                    case 2:
                      toast = _context.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "presentToastWithOptions",
          value: function presentToastWithOptions(data) {
            return (0, tslib__WEBPACK_IMPORTED_MODULE_2__.__awaiter)(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var toast;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return this.toastController.create({
                        header: 'Created new user.',
                        message: 'Id: ' + data['id'] + "\n" + 'Name: ' + data['name'] + "\n" + 'Job: ' + data['job'] + "\n" + 'Created at: ' + data['createdAt'] + "\n",
                        position: 'top',
                        buttons: [{
                          text: 'Done',
                          role: 'cancel',
                          handler: function handler() {
                            console.log('Cancel clicked');
                          }
                        }]
                      });

                    case 2:
                      toast = _context2.sent;
                      _context2.next = 5;
                      return toast.present();

                    case 5:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }]);

        return CreateUserPage;
      }();

      _CreateUserPage.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__.HttpClient
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__.ToastController
        }];
      };

      _CreateUserPage = (0, tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-create-user',
        template: _raw_loader_create_user_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_create_user_page_scss__WEBPACK_IMPORTED_MODULE_1__["default"]]
      })], _CreateUserPage);
      /***/
    },

    /***/
    45831:
    /*!***************************************************!*\
      !*** ./src/app/create-user/create-user.page.scss ***!
      \***************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".container {\n  margin: auto;\n  width: 75%;\n  padding: 16px;\n  background-color: white;\n}\n\n.email-select {\n  background: #f1f1f1;\n  border: none;\n  padding: 15px;\n  margin: 5px 0 22px 0;\n  width: 100%;\n}\n\ninput[type=text] {\n  width: 100%;\n  padding: 15px;\n  margin: 5px 0 22px 0;\n  display: inline-block;\n  border: none;\n  background: #f1f1f1;\n}\n\ninput[type=password] {\n  width: 100%;\n  padding: 15px;\n  margin: 5px 0 22px 0;\n  display: inline-block;\n  border: none;\n  background: #f1f1f1;\n}\n\n.loginButton {\n  color: white;\n  padding: 16px 20px;\n  margin: 8px 0;\n  border: none;\n  cursor: pointer;\n  width: 100%;\n  opacity: 0.8;\n  font-style: normal;\n  font-size: large;\n}\n\n.red {\n  background-color: #Eb445A;\n}\n\n.green {\n  background-color: #04AA6D;\n}\n\n.round {\n  border-radius: 8px;\n}\n\n.red:hover, .green:hover {\n  opacity: 1;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNyZWF0ZS11c2VyLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFlBQUE7RUFDQSxVQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0FBQ0o7O0FBSUU7RUFDRSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0Esb0JBQUE7RUFDQSxXQUFBO0FBREo7O0FBSUE7RUFDSSxXQUFBO0VBQ0EsYUFBQTtFQUNBLG9CQUFBO0VBQ0EscUJBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7QUFESjs7QUFJRTtFQUNJLFdBQUE7RUFDQSxhQUFBO0VBQ0Esb0JBQUE7RUFDQSxxQkFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtBQUROOztBQUtFO0VBQ0UsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0FBRko7O0FBS0U7RUFDRSx5QkFBQTtBQUZKOztBQUtFO0VBQ0UseUJBQUE7QUFGSjs7QUFLRTtFQUNFLGtCQUFBO0FBRko7O0FBS0U7RUFDRSxVQUFBO0FBRkoiLCJmaWxlIjoiY3JlYXRlLXVzZXIucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnRhaW5lcntcbiAgICBtYXJnaW46IGF1dG87XG4gICAgd2lkdGg6IDc1JTtcbiAgICBwYWRkaW5nOiAxNnB4O1xuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICAgXG4gIH1cbiAgXG4gIFxuICAuZW1haWwtc2VsZWN0e1xuICAgIGJhY2tncm91bmQ6ICNmMWYxZjE7XG4gICAgYm9yZGVyOiBub25lO1xuICAgIHBhZGRpbmc6IDE1cHg7XG4gICAgbWFyZ2luOiA1cHggMCAyMnB4IDA7XG4gICAgd2lkdGg6MTAwJTtcbiAgfVxuICBcbmlucHV0W3R5cGU9dGV4dF0ge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHBhZGRpbmc6IDE1cHg7XG4gICAgbWFyZ2luOiA1cHggMCAyMnB4IDA7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIGJvcmRlcjogbm9uZTtcbiAgICBiYWNrZ3JvdW5kOiAjZjFmMWYxO1xufVxuXG4gIGlucHV0W3R5cGU9cGFzc3dvcmRdIHtcbiAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgcGFkZGluZzogMTVweDtcbiAgICAgIG1hcmdpbjogNXB4IDAgMjJweCAwO1xuICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgICAgYm9yZGVyOiBub25lO1xuICAgICAgYmFja2dyb3VuZDogI2YxZjFmMTtcbiAgfVxuICBcbiAgXG4gIC5sb2dpbkJ1dHRvbntcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgcGFkZGluZzogMTZweCAyMHB4O1xuICAgIG1hcmdpbjogOHB4IDA7XG4gICAgYm9yZGVyOiBub25lO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBvcGFjaXR5OiAwLjg7XG4gICAgZm9udC1zdHlsZTpub3JtYWw7XG4gICAgZm9udC1zaXplOiBsYXJnZTtcbiAgfVxuICBcbiAgLnJlZHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRWI0NDVBO1xuICB9XG4gIFxuICAuZ3JlZW57XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzA0QUE2RDtcbiAgfVxuICBcbiAgLnJvdW5ke1xuICAgIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgfVxuICBcbiAgLnJlZDpob3ZlciwuZ3JlZW46aG92ZXJ7XG4gICAgb3BhY2l0eTogMTtcbiAgfVxuICBcbiAgXG4gIFxuICBcbiAgXG4gIFxuICAiXX0= */";
      /***/
    },

    /***/
    30671:
    /*!*****************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/create-user/create-user.page.html ***!
      \*****************************************************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Reqres.in Create user (POST)</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n\n\n<ion-content class=\"bg-generic\" >\n  <div>\n    <div class=\"container\">\n      <h1>Create new User</h1>\n      <p>Please fill this form to create a new user</p>\n      <hr>\n      <!--\n      <label for=\"name_create\"><b>Name</b></label>\n      <input type=\"text\" placeholder=\"Name\" required [(ngModel)]=\"create_name\" [ngModelOptions]=\"{standalone: true}\">\n      --> \n      <ion-item>\n        <ion-label position=\"floating\">Name</ion-label>\n        <ion-input type=\"text\" [(ngModel)]=\"create_name\"></ion-input>\n      </ion-item>\n      <!--  \n      <label for=\"job_create\"><b>Job</b></label>\n      <input type=\"text\" placeholder=\"Job\" required [(ngModel)]=\"create_job\" [ngModelOptions]=\"{standalone: true}\">\n      --> \n      <ion-item>\n        <ion-label position=\"floating\">Job</ion-label>\n        <ion-input [(ngModel)]=\"create_job\"></ion-input>\n      </ion-item>\n      <hr>\n      <div>\n        <ion-button (click)=\"sendPostData()\">\n          Create user \n        </ion-button>\n      </div>\n    </div>\n  </div>\n</ion-content>\n\n";
      /***/
    }
  }]);
})();
//# sourceMappingURL=src_app_create-user_create-user_module_ts-es5.js.map