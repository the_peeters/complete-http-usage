(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (self["webpackChunkIonicApiRequest"] = self["webpackChunkIonicApiRequest"] || []).push([["src_app_register-user_register-user_module_ts"], {
    /***/
    91481:
    /*!***************************************************************!*\
      !*** ./src/app/register-user/register-user-routing.module.ts ***!
      \***************************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "RegisterUserPageRoutingModule": function RegisterUserPageRoutingModule() {
          return (
            /* binding */
            _RegisterUserPageRoutingModule
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! tslib */
      61855);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      42741);
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      29535);
      /* harmony import */


      var _register_user_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./register-user.page */
      92610);

      var routes = [{
        path: '',
        component: _register_user_page__WEBPACK_IMPORTED_MODULE_0__.RegisterUserPage
      }];

      var _RegisterUserPageRoutingModule = function RegisterUserPageRoutingModule() {
        _classCallCheck(this, RegisterUserPageRoutingModule);
      };

      _RegisterUserPageRoutingModule = (0, tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule]
      })], _RegisterUserPageRoutingModule);
      /***/
    },

    /***/
    13963:
    /*!*******************************************************!*\
      !*** ./src/app/register-user/register-user.module.ts ***!
      \*******************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "RegisterUserPageModule": function RegisterUserPageModule() {
          return (
            /* binding */
            _RegisterUserPageModule
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! tslib */
      61855);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      42741);
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/common */
      16274);
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/forms */
      93324);
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic/angular */
      34595);
      /* harmony import */


      var _register_user_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./register-user-routing.module */
      91481);
      /* harmony import */


      var _register_user_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./register-user.page */
      92610);

      var _RegisterUserPageModule = function RegisterUserPageModule() {
        _classCallCheck(this, RegisterUserPageModule);
      };

      _RegisterUserPageModule = (0, tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule, _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule, _register_user_routing_module__WEBPACK_IMPORTED_MODULE_0__.RegisterUserPageRoutingModule, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.ReactiveFormsModule],
        declarations: [_register_user_page__WEBPACK_IMPORTED_MODULE_1__.RegisterUserPage]
      })], _RegisterUserPageModule);
      /***/
    },

    /***/
    92610:
    /*!*****************************************************!*\
      !*** ./src/app/register-user/register-user.page.ts ***!
      \*****************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "RegisterUserPage": function RegisterUserPage() {
          return (
            /* binding */
            _RegisterUserPage
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! tslib */
      61855);
      /* harmony import */


      var _raw_loader_register_user_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! !raw-loader!./register-user.page.html */
      40554);
      /* harmony import */


      var _register_user_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./register-user.page.scss */
      67612);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @angular/core */
      42741);
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/common/http */
      31887);
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic/angular */
      34595);
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      93324);
      /* harmony import */


      var _services_managers_users_manager_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../services/managers/users-manager.service */
      29022); // add  ReactiveFormsModule to register-page.modules.ts


      var _RegisterUserPage = /*#__PURE__*/function () {
        function RegisterUserPage(http, toastController, usersManager) {
          _classCallCheck(this, RegisterUserPage);

          this.http = http;
          this.toastController = toastController;
          this.usersManager = usersManager;
          this.users_login = new Array();
          this.selectControl = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__.FormControl();
        }

        _createClass(RegisterUserPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "ionViewWillEnter",
          value: function ionViewWillEnter() {
            return (0, tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.usersManager.getUsers();

                    case 2:
                      this.users_login = _context.sent;

                    case 3:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "successfulRegister",
          value: function successfulRegister() {
            var _this = this;

            if (this.register_password1 && this.register_password2 && this.selectControl.value) {
              if (this.register_password1 != this.register_password2) {
                this.presentFailureToast('Passwords does not match');
                return;
              }

              var postData = {
                "email": this.selectControl.value,
                "password": this.register_password2
              };
              console.log(postData);
              this.http.post("https://reqres.in/api/register", postData).subscribe(function (data) {
                console.log(data);
                var msg = 'Id: ' + data['id'] + "\n" + 'Token: ' + data['token'];

                _this.presentSuccessfulToast('Succesful Registration', msg);
              }, function (error) {
                _this.presentFailureToast('Error during registration');

                console.log(error);
              });
            } else {
              this.presentFailureToast('Cannot make successful registration with empty values');
            }
          }
        }, {
          key: "failureRegister",
          value: function failureRegister() {
            var _this2 = this;

            if (this.selectControl.value === null) {
              console.log(this.selectControl.value);
              this.presentFailureToast('Cannot make UNsuccessful registration without user email');
              return;
            }

            if (!this.register_password1 && !this.register_password2) {
              var postData = {
                "email": this.selectControl.value
              };
              console.log(postData);
              this.http.post("https://reqres.in/api/register", postData).subscribe(function (data) {
                console.log(data);

                _this2.presentFailureToast('It was supposed it throws an error');
              }, function (error) {
                var msg = 'Reason: ' + error['error']['error'];

                _this2.presentSuccessfulToast('Unsuccesful Registration', msg);

                console.log();
              });
            } else {
              this.presentFailureToast('Cannot make UNsuccessful registration with passsword value');
            }
          }
        }, {
          key: "presentFailureToast",
          value: function presentFailureToast(msg) {
            return (0, tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var toast;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return this.toastController.create({
                        message: msg,
                        position: 'top',
                        duration: 2000
                      });

                    case 2:
                      toast = _context2.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "presentSuccessfulToast",
          value: function presentSuccessfulToast(header, msg) {
            return (0, tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              var toast;
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      _context3.next = 2;
                      return this.toastController.create({
                        header: header,
                        message: msg,
                        position: 'top',
                        buttons: [{
                          text: 'Done',
                          role: 'cancel',
                          handler: function handler() {
                            console.log('Cancel clicked');
                          }
                        }]
                      });

                    case 2:
                      toast = _context3.sent;
                      _context3.next = 5;
                      return toast.present();

                    case 5:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }
        }]);

        return RegisterUserPage;
      }();

      _RegisterUserPage.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_5__.HttpClient
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.ToastController
        }, {
          type: _services_managers_users_manager_service__WEBPACK_IMPORTED_MODULE_2__.UsersManagerService
        }];
      };

      _RegisterUserPage = (0, tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-register-user',
        template: _raw_loader_register_user_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_register_user_page_scss__WEBPACK_IMPORTED_MODULE_1__["default"]]
      })], _RegisterUserPage);
      /***/
    },

    /***/
    67612:
    /*!*******************************************************!*\
      !*** ./src/app/register-user/register-user.page.scss ***!
      \*******************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".container {\n  margin: auto;\n  width: 75%;\n  padding: 16px;\n  background-color: white;\n}\n\n.email-select {\n  background: #f1f1f1;\n  border: none;\n  padding: 15px;\n  margin: 5px 0 22px 0;\n  width: 100%;\n}\n\ninput[type=password] {\n  width: 100%;\n  padding: 15px;\n  margin: 5px 0 22px 0;\n  display: inline-block;\n  border: none;\n  background: #f1f1f1;\n}\n\ninput[type=password]:focus {\n  background: #ddd;\n}\n\n.loginButton {\n  color: white;\n  padding: 16px 20px;\n  margin: 8px 0;\n  border: none;\n  cursor: pointer;\n  width: 100%;\n  opacity: 0.8;\n  font-style: normal;\n  font-size: large;\n}\n\n.red {\n  background-color: #Eb445A;\n}\n\n.green {\n  background-color: #04AA6D;\n}\n\n.round {\n  border-radius: 8px;\n}\n\n.red:hover, .green:hover {\n  opacity: 1;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInJlZ2lzdGVyLXVzZXIucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsWUFBQTtFQUNBLFVBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7QUFDRjs7QUFJQTtFQUNFLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSxvQkFBQTtFQUNBLFdBQUE7QUFERjs7QUFLQTtFQUNJLFdBQUE7RUFDQSxhQUFBO0VBQ0Esb0JBQUE7RUFDQSxxQkFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtBQUZKOztBQUtBO0VBQ0UsZ0JBQUE7QUFGRjs7QUFNQTtFQUNFLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQUhGOztBQU1BO0VBQ0UseUJBQUE7QUFIRjs7QUFNQTtFQUNFLHlCQUFBO0FBSEY7O0FBTUE7RUFDRSxrQkFBQTtBQUhGOztBQU1BO0VBQ0UsVUFBQTtBQUhGIiwiZmlsZSI6InJlZ2lzdGVyLXVzZXIucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnRhaW5lcntcbiAgbWFyZ2luOiBhdXRvO1xuICB3aWR0aDogNzUlO1xuICBwYWRkaW5nOiAxNnB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiBcbn1cblxuXG4uZW1haWwtc2VsZWN0e1xuICBiYWNrZ3JvdW5kOiAjZjFmMWYxO1xuICBib3JkZXI6IG5vbmU7XG4gIHBhZGRpbmc6IDE1cHg7XG4gIG1hcmdpbjogNXB4IDAgMjJweCAwO1xuICB3aWR0aDoxMDAlO1xufVxuXG5cbmlucHV0W3R5cGU9cGFzc3dvcmRdIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwYWRkaW5nOiAxNXB4O1xuICAgIG1hcmdpbjogNXB4IDAgMjJweCAwO1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICBib3JkZXI6IG5vbmU7XG4gICAgYmFja2dyb3VuZDogI2YxZjFmMTtcbn1cblxuaW5wdXRbdHlwZT1wYXNzd29yZF06Zm9jdXN7XG4gIGJhY2tncm91bmQ6ICNkZGQ7XG59XG5cblxuLmxvZ2luQnV0dG9ue1xuICBjb2xvcjogd2hpdGU7XG4gIHBhZGRpbmc6IDE2cHggMjBweDtcbiAgbWFyZ2luOiA4cHggMDtcbiAgYm9yZGVyOiBub25lO1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIHdpZHRoOiAxMDAlO1xuICBvcGFjaXR5OiAwLjg7XG4gIGZvbnQtc3R5bGU6bm9ybWFsO1xuICBmb250LXNpemU6IGxhcmdlO1xufVxuXG4ucmVke1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjRWI0NDVBO1xufVxuXG4uZ3JlZW57XG4gIGJhY2tncm91bmQtY29sb3I6ICMwNEFBNkQ7XG59XG5cbi5yb3VuZHtcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xufVxuXG4ucmVkOmhvdmVyLC5ncmVlbjpob3ZlcntcbiAgb3BhY2l0eTogMTtcbn1cblxuXG5cblxuXG5cbiJdfQ== */";
      /***/
    },

    /***/
    40554:
    /*!*********************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/register-user/register-user.page.html ***!
      \*********************************************************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar  color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Reqres.in Register user (POST)</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content  class=\"bg-generic\"> \n  <form>\n    <div class=\"container\">\n      <h1>Register into Reqres.in</h1>\n      <p>Please fill in this form to create an account. (Note: Only defined users succeed registration)</p>\n      <hr>\n  \n      <label for=\"email\"><b>Email</b></label>\n      <div>\n        <select class=\"email-select\" [formControl]=\"selectControl\">\n          <option value={{user.email}} *ngFor=\"let user of users_login\" > {{user.email}}</option>\n        </select>\n    \n      </div>\n  \n      <label for=\"psw\"><b>Password</b></label>\n      <input type=\"password\" placeholder=\"Enter Password\" required [(ngModel)]=\"register_password1\" [ngModelOptions]=\"{standalone: true}\">\n\n      <label for=\"psw\"><b>Password confirm</b></label>\n      <input type=\"password\" placeholder=\"Enter Password\" required [(ngModel)]=\"register_password2\" [ngModelOptions]=\"{standalone: true}\">\n\n      <hr>\n      <div>\n        <button type=\"submit\" class=\"loginButton green round\" (click)=\"successfulRegister()\">Register with success</button>\n        <button type=\"submit\" class=\"loginButton red round\" (click)=\"failureRegister()\">Register with a failure</button>\n      </div>\n    </div>\n  </form>\n\n</ion-content>\n";
      /***/
    }
  }]);
})();
//# sourceMappingURL=src_app_register-user_register-user_module_ts-es5.js.map