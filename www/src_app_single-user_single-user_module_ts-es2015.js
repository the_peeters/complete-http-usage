(self["webpackChunkIonicApiRequest"] = self["webpackChunkIonicApiRequest"] || []).push([["src_app_single-user_single-user_module_ts"],{

/***/ 98317:
/*!***********************************************************!*\
  !*** ./src/app/single-user/single-user-routing.module.ts ***!
  \***********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SingleUserPageRoutingModule": function() { return /* binding */ SingleUserPageRoutingModule; }
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 29535);
/* harmony import */ var _single_user_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./single-user.page */ 97714);




const routes = [
    {
        path: '',
        component: _single_user_page__WEBPACK_IMPORTED_MODULE_0__.SingleUserPage
    }
];
let SingleUserPageRoutingModule = class SingleUserPageRoutingModule {
};
SingleUserPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], SingleUserPageRoutingModule);



/***/ }),

/***/ 25216:
/*!***************************************************!*\
  !*** ./src/app/single-user/single-user.module.ts ***!
  \***************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SingleUserPageModule": function() { return /* binding */ SingleUserPageModule; }
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 16274);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 93324);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 34595);
/* harmony import */ var _single_user_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./single-user-routing.module */ 98317);
/* harmony import */ var _single_user_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./single-user.page */ 97714);







let SingleUserPageModule = class SingleUserPageModule {
};
SingleUserPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _single_user_routing_module__WEBPACK_IMPORTED_MODULE_0__.SingleUserPageRoutingModule
        ],
        declarations: [_single_user_page__WEBPACK_IMPORTED_MODULE_1__.SingleUserPage]
    })
], SingleUserPageModule);



/***/ }),

/***/ 97714:
/*!*************************************************!*\
  !*** ./src/app/single-user/single-user.page.ts ***!
  \*************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SingleUserPage": function() { return /* binding */ SingleUserPage; }
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _raw_loader_single_user_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./single-user.page.html */ 86517);
/* harmony import */ var _single_user_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./single-user.page.scss */ 6453);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ 31887);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ 29535);






let SingleUserPage = class SingleUserPage {
    constructor(route, http) {
        this.route = route;
        this.http = http;
        this.titlePage = 'Default detail user: George Bluth';
    }
    ngOnInit() {
        this.route.params.subscribe((params) => {
            let user_id = (params['id']) ? params['id'] : 1;
            let default_ = (params['id']) ? false : true;
            this.listUsers = this.http.get("https://reqres.in/api/users/" + user_id);
            this.listUsers.subscribe(data => {
                if (default_ == true) {
                    this.titlePage = 'Default detail user: George Bluth';
                }
                else {
                    this.titlePage = data['data']['first_name'] + " " + data['data']['last_name'];
                }
            });
        });
    }
};
SingleUserPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__.ActivatedRoute },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__.HttpClient }
];
SingleUserPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-single-user',
        template: _raw_loader_single_user_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_single_user_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], SingleUserPage);



/***/ }),

/***/ 6453:
/*!***************************************************!*\
  !*** ./src/app/single-user/single-user.page.scss ***!
  \***************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzaW5nbGUtdXNlci5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ 86517:
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/single-user/single-user.page.html ***!
  \*****************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar color=\"primary\"> \n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title >{{titlePage}}</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-item *ngIf=\"(listUsers | async)?.data as user\">\n    <p>{{ user.first_name }}  {{ user.last_name }}</p>\n  </ion-item>\n</ion-content>\n");

/***/ })

}]);
//# sourceMappingURL=src_app_single-user_single-user_module_ts-es2015.js.map