(self["webpackChunkIonicApiRequest"] = self["webpackChunkIonicApiRequest"] || []).push([["src_app_login-page_login-page_module_ts"],{

/***/ 86825:
/*!*********************************************************!*\
  !*** ./src/app/login-page/login-page-routing.module.ts ***!
  \*********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoginPagePageRoutingModule": function() { return /* binding */ LoginPagePageRoutingModule; }
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 29535);
/* harmony import */ var _login_page_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./login-page.page */ 41552);




const routes = [
    {
        path: '',
        component: _login_page_page__WEBPACK_IMPORTED_MODULE_0__.LoginPagePage
    }
];
let LoginPagePageRoutingModule = class LoginPagePageRoutingModule {
};
LoginPagePageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], LoginPagePageRoutingModule);



/***/ }),

/***/ 34297:
/*!*************************************************!*\
  !*** ./src/app/login-page/login-page.module.ts ***!
  \*************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoginPagePageModule": function() { return /* binding */ LoginPagePageModule; }
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 16274);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 93324);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 34595);
/* harmony import */ var _login_page_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./login-page-routing.module */ 86825);
/* harmony import */ var _login_page_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./login-page.page */ 41552);







let LoginPagePageModule = class LoginPagePageModule {
};
LoginPagePageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.ReactiveFormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _login_page_routing_module__WEBPACK_IMPORTED_MODULE_0__.LoginPagePageRoutingModule
        ],
        declarations: [_login_page_page__WEBPACK_IMPORTED_MODULE_1__.LoginPagePage]
    })
], LoginPagePageModule);



/***/ }),

/***/ 41552:
/*!***********************************************!*\
  !*** ./src/app/login-page/login-page.page.ts ***!
  \***********************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoginPagePage": function() { return /* binding */ LoginPagePage; }
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _raw_loader_login_page_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./login-page.page.html */ 28203);
/* harmony import */ var _login_page_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./login-page.page.scss */ 92742);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ 31887);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 34595);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ 93324);
/* harmony import */ var _services_managers_users_manager_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/managers/users-manager.service */ 29022);








let LoginPagePage = class LoginPagePage {
    constructor(http, toastController, usersManager) {
        this.http = http;
        this.toastController = toastController;
        this.usersManager = usersManager;
        this.selectControl = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__.FormControl();
    }
    ngOnInit() {
    }
    ionViewWillEnter() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.users_login = yield this.usersManager.getUsers();
            /*this.users_login= new Array();
            this.http.get("https://reqres.in/api/users/").subscribe(data=>{
              this.users_login=data['data'];
            });*/
        });
    }
    successfulLogin() {
        if (this.login_password && this.selectControl.value) {
            let postData = {
                "email": this.selectControl.value,
                "password": this.login_password
            };
            console.log(postData);
            this.http.post("https://reqres.in/api/login", postData)
                .subscribe(data => {
                console.log(data);
                const msg = 'Token: ' + data['token'];
                this.presentSuccessfulToast('Succesful Login', msg);
            }, error => {
                this.presentFailureToast('Error during registration');
                console.log(error);
            });
        }
        else {
            this.presentFailureToast('Cannot make successful registration with empty values');
        }
    }
    failureLogin() {
        if (this.selectControl.value && !this.login_password) {
            let postData = {
                "email": "sydney@fife",
            };
            console.log(postData);
            this.http.post("https://reqres.in/api/register", postData)
                .subscribe(data => {
                console.log(data);
                this.presentFailureToast('It was supposed it throws an error');
            }, error => {
                const msg = 'Reason: ' + error['error']['error'];
                this.presentSuccessfulToast('Login failure', msg);
                console.log();
            });
        }
        else {
            this.presentFailureToast('Cannot make UNsuccessful registration with passsword value or without email');
        }
    }
    presentFailureToast(msg) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: msg,
                position: 'top',
                duration: 2000
            });
            toast.present();
        });
    }
    presentSuccessfulToast(header, msg) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                header: header,
                message: msg,
                position: 'top',
                buttons: [
                    {
                        text: 'Done',
                        role: 'cancel',
                        handler: () => {
                            console.log('Cancel clicked');
                        }
                    }
                ]
            });
            yield toast.present();
        });
    }
};
LoginPagePage.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_5__.HttpClient },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.ToastController },
    { type: _services_managers_users_manager_service__WEBPACK_IMPORTED_MODULE_2__.UsersManagerService }
];
LoginPagePage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-login-page',
        template: _raw_loader_login_page_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_login_page_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], LoginPagePage);



/***/ }),

/***/ 92742:
/*!*************************************************!*\
  !*** ./src/app/login-page/login-page.page.scss ***!
  \*************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container {\n  margin: auto;\n  width: 75%;\n  padding: 16px;\n  background-color: white;\n}\n\n.email-select {\n  background: #f1f1f1;\n  border: none;\n  padding: 15px;\n  margin: 5px 0 22px 0;\n  width: 100%;\n}\n\ninput[type=password] {\n  width: 100%;\n  padding: 15px;\n  margin: 5px 0 22px 0;\n  display: inline-block;\n  border: none;\n  background: #f1f1f1;\n}\n\ninput[type=password]:focus {\n  background: #ddd;\n}\n\n.loginButton {\n  color: white;\n  padding: 16px 20px;\n  margin: 8px 0;\n  border: none;\n  cursor: pointer;\n  width: 100%;\n  opacity: 0.8;\n  font-style: normal;\n  font-size: large;\n}\n\n.red {\n  background-color: #Eb445A;\n}\n\n.green {\n  background-color: #04AA6D;\n}\n\n.round {\n  border-radius: 8px;\n}\n\n.red:hover, .green:hover {\n  opacity: 1;\n}\n\n#login_form {\n  display: table;\n  margin: auto;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxvZ2luLXBhZ2UucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsWUFBQTtFQUNBLFVBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7QUFDRjs7QUFJQTtFQUNFLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSxvQkFBQTtFQUNBLFdBQUE7QUFERjs7QUFLQTtFQUNFLFdBQUE7RUFDQSxhQUFBO0VBQ0Esb0JBQUE7RUFDQSxxQkFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtBQUZGOztBQUtBO0VBQ0UsZ0JBQUE7QUFGRjs7QUFNQTtFQUNFLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQUhGOztBQU1BO0VBQ0UseUJBQUE7QUFIRjs7QUFNQTtFQUNFLHlCQUFBO0FBSEY7O0FBTUE7RUFDRSxrQkFBQTtBQUhGOztBQU1BO0VBQ0UsVUFBQTtBQUhGOztBQU1BO0VBQ0UsY0FBQTtFQUNBLFlBQUE7QUFIRiIsImZpbGUiOiJsb2dpbi1wYWdlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb250YWluZXJ7ICBcbiAgbWFyZ2luOiBhdXRvO1xuICB3aWR0aDogNzUlO1xuICBwYWRkaW5nOiAxNnB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgIFxufVxuICBcbiAgXG4uZW1haWwtc2VsZWN0e1xuICBiYWNrZ3JvdW5kOiAjZjFmMWYxO1xuICBib3JkZXI6IG5vbmU7XG4gIHBhZGRpbmc6IDE1cHg7XG4gIG1hcmdpbjogNXB4IDAgMjJweCAwO1xuICB3aWR0aDoxMDAlO1xufVxuICBcbiAgXG5pbnB1dFt0eXBlPXBhc3N3b3JkXSB7XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nOiAxNXB4O1xuICBtYXJnaW46IDVweCAwIDIycHggMDtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBib3JkZXI6IG5vbmU7XG4gIGJhY2tncm91bmQ6ICNmMWYxZjE7XG59XG4gIFxuaW5wdXRbdHlwZT1wYXNzd29yZF06Zm9jdXN7XG4gIGJhY2tncm91bmQ6ICNkZGQ7XG59XG4gIFxuICBcbi5sb2dpbkJ1dHRvbntcbiAgY29sb3I6IHdoaXRlO1xuICBwYWRkaW5nOiAxNnB4IDIwcHg7XG4gIG1hcmdpbjogOHB4IDA7XG4gIGJvcmRlcjogbm9uZTtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICB3aWR0aDogMTAwJTtcbiAgb3BhY2l0eTogMC44O1xuICBmb250LXN0eWxlOm5vcm1hbDtcbiAgZm9udC1zaXplOiBsYXJnZTtcbn1cbiAgXG4ucmVke1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjRWI0NDVBO1xufVxuICBcbi5ncmVlbntcbiAgYmFja2dyb3VuZC1jb2xvcjogIzA0QUE2RDtcbn1cbiAgXG4ucm91bmR7XG4gIGJvcmRlci1yYWRpdXM6IDhweDtcbn1cbiAgXG4ucmVkOmhvdmVyLC5ncmVlbjpob3ZlcntcbiAgb3BhY2l0eTogMTtcbn1cblxuI2xvZ2luX2Zvcm17XG4gIGRpc3BsYXk6IHRhYmxlO1xuICBtYXJnaW46IGF1dG87XG59IFxuICAiXX0= */");

/***/ }),

/***/ 28203:
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/login-page/login-page.page.html ***!
  \***************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar  color=\"primary\">\n    <ion-buttons  slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Login user</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n\n<ion-content class=\"background-image\">\n  <form id=\"login_form\" >\n\n    <div class=\"container\"  >\n      <h1>Login toReqres.in</h1>\n      <p> Note: Only defined users</p>\n      <hr>\n  \n      <label for=\"email\"><b>Email</b></label>\n      <div>\n        <select class=\"email-select\" [formControl]=\"selectControl\">\n          <option value={{user.email}} *ngFor=\"let user of users_login\" > {{user.email}}</option>\n        </select>\n    \n      </div>\n  \n      <label for=\"psw\"><b>Password</b></label>\n      <input type=\"password\" placeholder=\"Enter Password\" required [(ngModel)]=\"login_password\" [ngModelOptions]=\"{standalone: true}\">\n\n      <hr>\n      <div>\n        <button type=\"submit\" class=\"loginButton green round\" (click)=\"successfulLogin()\">Register with success</button>\n        <button type=\"submit\" class=\"loginButton red round\" (click)=\"failureLogin()\">Register with a failure</button>\n      </div>\n    </div>\n  </form>\n\n</ion-content>\n");

/***/ })

}]);
//# sourceMappingURL=src_app_login-page_login-page_module_ts-es2015.js.map