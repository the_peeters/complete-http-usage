(self["webpackChunkIonicApiRequest"] = self["webpackChunkIonicApiRequest"] || []).push([["src_app_delete-user_delete-user_module_ts"],{

/***/ 3148:
/*!***********************************************************!*\
  !*** ./src/app/delete-user/delete-user-routing.module.ts ***!
  \***********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DeleteUserPageRoutingModule": function() { return /* binding */ DeleteUserPageRoutingModule; }
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 29535);
/* harmony import */ var _delete_user_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./delete-user.page */ 72743);




const routes = [
    {
        path: '',
        component: _delete_user_page__WEBPACK_IMPORTED_MODULE_0__.DeleteUserPage
    }
];
let DeleteUserPageRoutingModule = class DeleteUserPageRoutingModule {
};
DeleteUserPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], DeleteUserPageRoutingModule);



/***/ }),

/***/ 11059:
/*!***************************************************!*\
  !*** ./src/app/delete-user/delete-user.module.ts ***!
  \***************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DeleteUserPageModule": function() { return /* binding */ DeleteUserPageModule; }
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 16274);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 93324);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 34595);
/* harmony import */ var _delete_user_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./delete-user-routing.module */ 3148);
/* harmony import */ var _delete_user_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./delete-user.page */ 72743);







let DeleteUserPageModule = class DeleteUserPageModule {
};
DeleteUserPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _delete_user_routing_module__WEBPACK_IMPORTED_MODULE_0__.DeleteUserPageRoutingModule
        ],
        declarations: [_delete_user_page__WEBPACK_IMPORTED_MODULE_1__.DeleteUserPage]
    })
], DeleteUserPageModule);



/***/ }),

/***/ 72743:
/*!*************************************************!*\
  !*** ./src/app/delete-user/delete-user.page.ts ***!
  \*************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DeleteUserPage": function() { return /* binding */ DeleteUserPage; }
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 61855);
/* harmony import */ var _raw_loader_delete_user_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./delete-user.page.html */ 93476);
/* harmony import */ var _delete_user_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./delete-user.page.scss */ 53100);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ 31887);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 42741);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 29535);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ 34595);







let DeleteUserPage = class DeleteUserPage {
    constructor(http, toastController, route) {
        this.http = http;
        this.toastController = toastController;
        this.route = route;
        this.titlePage = '(Default) Delete George Bluth';
    }
    ngOnInit() {
        this.route.params.subscribe((params) => {
            if (params['id']) {
                this.id = params['id'];
                this.user = this.http.get("https://reqres.in/api/users/" + params['id']);
                this.user.subscribe(data => {
                    this.titlePage = "Delete " + data['data']['first_name'] + " " + data['data']['last_name'];
                });
            }
        });
    }
    deleteUser() {
        this.id = (this.id == undefined) ? 1 : this.user;
        this.http.delete("https://reqres.in/api/users/" + this.id)
            .subscribe(data => {
            console.log(data);
            this.presentSuccessfulToast('Succesful deletion', "");
        }, error => {
            this.presentFailureToast('Error during the delete process');
            console.log(error);
        });
    }
    presentFailureToast(msg) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__awaiter)(this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: msg,
                position: 'top',
                duration: 2000
            });
            toast.present();
        });
    }
    presentSuccessfulToast(header, msg) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__awaiter)(this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                header: header,
                message: msg,
                position: 'top',
                buttons: [
                    {
                        text: 'Done',
                        role: 'cancel',
                        handler: () => {
                            console.log('Cancel clicked');
                        }
                    }
                ]
            });
            yield toast.present();
        });
    }
};
DeleteUserPage.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__.HttpClient },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__.ToastController },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__.ActivatedRoute }
];
DeleteUserPage = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-delete-user',
        template: _raw_loader_delete_user_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_delete_user_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], DeleteUserPage);



/***/ }),

/***/ 53100:
/*!***************************************************!*\
  !*** ./src/app/delete-user/delete-user.page.scss ***!
  \***************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJkZWxldGUtdXNlci5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ 93476:
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/delete-user/delete-user.page.html ***!
  \*****************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title >{{titlePage}}</ion-title>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  \n\n\n  \n<ion-content>\n  <ion-item>\n    <ion-button fill=\"solid\" color=\"primary\" routerLink=\"/mainpage\"> Cancel </ion-button>\n    <ion-button fill=\"solid\" color=\"danger\" (click)=\"deleteUser()\"> Delete </ion-button>\n  </ion-item>\n</ion-content>\n");

/***/ })

}]);
//# sourceMappingURL=src_app_delete-user_delete-user_module_ts-es2015.js.map